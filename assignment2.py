from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)

books = {1:'Software Engineering',2:'Algorithm Design',3:'Python'}

import json
@app.route('/book/JSON')
def bookJSON():
    bookJSON = []
    for book in books:
        bookJSON.append({"title": str(books[int(book)]), "id": str(book)})
    return json.dumps(bookJSON, indent=4)
    # <your code>
    

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)
    # <your code>
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    return render_template('newBook.html')
    # <your code>

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    return render_template('editBook.html', book_id = book_id, books = books)

    # <your code>
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    return render_template('deleteBook.html', book_id = book_id, books = books)

    # <your code>
    
@app.route('/handle_edit', methods=['POST'])
def handle_edit():
  
    newName = request.form['newName']
    book_id = request.form['book_id']
    if newName == "":
        pass
    else:
        books[int(book_id)] = newName
    return render_template('showBook.html', books = books)
    
@app.route('/handle_delete', methods=['POST'])
def handle_delete():
    book_id = request.form['book_id']
    del books[int(book_id)]
    return render_template('showBook.html', books = books)

@app.route('/handle_new', methods=['POST'])
def handle_new():
    newBook = request.form['newBook']
    index = 0
    found = False
    
    maxId = 1
    for book in books:
        if book > maxId:
            maxId = book
    
    while(found == False):
        index+=1 
        isIn = False
        for book in books:
            if book == index:
                isIn = True
        if(isIn == False):
            found = True

    books[index] = newBook
        
    return render_template('showBook.html', books = books)
 
    
if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

